import React, { Component } from 'react';
import './App.scss';
import TodoItem from './components/TodoItem';

import untickAll from './assets/img/untick-all.svg'
import tickAll from './assets/img/tick-all.svg'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toDoItems: [
        { title: 'Learn English with Elsa', status: false },
        { title: 'Go to cinema with my girlfriend', status: false },
        { title: 'Have a nice dinner', status: false }
      ]
    }
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  myCheckParent(item) {
    this.setState({
      toDoItems: this.state.toDoItems.map((toDoItem) => {
        if (toDoItem != item) {
          return { ...toDoItem }
        } else {
          return { ...toDoItem, status: !item.status }
        }
      })
    })
  }

  onKeyUp(event) {
    let text = event.target.value;
    if (event.keyCode === 13) {
      if (!text || text == "") {
        return;
      }
      this.setState({
        toDoItems: [
          { title: text, status: false },
          ...this.state.toDoItems
        ]
      })
    }
  }

  render() {
    const { toDoItems } = this.state

    if (toDoItems.length != 0) {
      return (
        <div className="App">

          <div className="Header">
            <img className="Header__tickall" src={untickAll} width="32px" />

            <div className="Group">
              <input type="text" 
              placeholder="Press enter your to do..."
              className="Group__addtodo" 
              required 
              onKeyUp={this.onKeyUp} />
            </div>
          </div>

          {toDoItems.map((item, index) =>
            <TodoItem
              key={'item:' + index}
              item={item}
              myCheck={() => this.myCheckParent(item)} />)}
        </div>
      );
    } else {
      return (
        <div className="App">
          <p>Nothing here!!!</p>
        </div>
      );
    }
  }
}

export default App;