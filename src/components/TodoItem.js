import React, { Component } from 'react';
import './TodoItem.scss'
import classNames from 'classnames';
import checked from '../assets/img/checked.svg'
import uncheck from '../assets/img/uncheck.svg'

import PropTypes from 'prop-types';

class TodoItem extends Component {
    constructor(props){
        super(props);
    }
    
    render() {
        const {item,myCheck} = this.props;
        console.log(this.props);
        const classCompleted = classNames({'-completed':item.status})

        let checkImg = uncheck;
        if(item.status){
            checkImg = checked
        }

        return (
            <div className="todoitem">
              <img onClick={myCheck} src={checkImg} width="26px" />
              <p className={"todoitem__title "+ classCompleted} >{item.title} </p>  
            </div>
        );
    }
}

TodoItem.propTypes = {
    item:PropTypes.shape({
        title:PropTypes.string.isRequired,
        status: PropTypes.bool.isRequired
    }),
    myCheck: PropTypes.func
}

export default TodoItem;